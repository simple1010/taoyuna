$(window).load(function(){
	$('.character-group').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
		    {
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
		    },
		    {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
		    }
		]
	});
	var _windowW = $(window).width();
	//console.log(_windowW);
	if ( _windowW >= (1200-16)){
		TweenMax.to('.nav',1, {right:'0',ease: Power2.easeOut});
		TweenMax.to('.nav',1, {right:'-20px',ease: Power2.easeOut,delay:1});
		TweenMax.to('.slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.page-slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.logo',1, {top:'80px',opacity:1,ease: Power2.easeOut,delay:1.5});
		TweenMax.to('.ani-plain',5, {left:'40%',bottom:'60px',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});

		TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'42%',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});

		function _loopAni(){
			$('.ani-plain').removeAttr('style');
			$('.ani-plain-shadow').removeAttr('style');
			TweenMax.to('.ani-plain',5, {left:'40%',bottom:'60px',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});
			TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'42%',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});
		}
	}

	if ( _windowW >= (1000-16) && _windowW <= 1199 ){
		TweenMax.to('.nav',1, {right:'0',ease: Power2.easeOut});
		TweenMax.to('.nav',1, {right:'-20px',ease: Power2.easeOut,delay:1});
		TweenMax.to('.slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.page-slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.logo',1, {top:'47px',opacity:1,ease: Power2.easeOut,delay:1.5});
		TweenMax.to('.ani-plain',5, {left:'40%',bottom:'60px',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});

		TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'42%',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});

		function _loopAni(){
			$('.ani-plain').removeAttr('style');
			$('.ani-plain-shadow').removeAttr('style');
			TweenMax.to('.ani-plain',5, {left:'40%',bottom:'60px',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});
			TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'42%',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});
		}
	}

	if ( _windowW >= (768-16) && _windowW <= 999){
		TweenMax.to('.slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.page-slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.ani-plain',5, {left:'32%',bottom:'60px',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});

		TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'36%',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});

		function _loopAni(){
			$('.ani-plain').removeAttr('style');
			$('.ani-plain-shadow').removeAttr('style');
			TweenMax.to('.ani-plain',5, {left:'32%',bottom:'60px',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain',5, {left:'100%',bottom:'150px',ease: Power2.easeIn,delay:7.5});
			TweenMax.to('.ani-plain-shadow',5, {width:'240px',left:'36%',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});
		}
	}

	if ( _windowW >= (321-16) && _windowW <= 767){
		TweenMax.to('.slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.page-slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.ani-plain',5, {left:'28%',bottom:'25px',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain',5, {left:'100%',bottom:'25px',ease: Power2.easeIn,delay:7.5});

		TweenMax.to('.ani-plain-shadow',5, {width:'150px',left:'36%',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});

		function _loopAni(){
			$('.ani-plain').removeAttr('style');
			$('.ani-plain-shadow').removeAttr('style');
			TweenMax.to('.ani-plain',5, {left:'28%',bottom:'25px',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain',5, {left:'100%',bottom:'25px',ease: Power2.easeIn,delay:7.5});
			TweenMax.to('.ani-plain-shadow',5, {width:'150px',left:'36%',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});
		}
	}

	if ( _windowW <= 320){
		TweenMax.to('.slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.page-slogan-box',1, {top:'0',opacity:1,ease: Power2.easeOut,delay:1});
		TweenMax.to('.ani-plain',5, {left:'22%',bottom:'25px',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain',5, {left:'100%',bottom:'25px',ease: Power2.easeIn,delay:7.5});

		TweenMax.to('.ani-plain-shadow',5, {width:'150px',left:'30%',ease: Power2.easeOut,delay:0.5});
		TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});

		function _loopAni(){
			$('.ani-plain').removeAttr('style');
			$('.ani-plain-shadow').removeAttr('style');
			TweenMax.to('.ani-plain',5, {left:'22%',bottom:'25px',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain',5, {left:'100%',bottom:'25px',ease: Power2.easeIn,delay:7.5});
			TweenMax.to('.ani-plain-shadow',5, {width:'150px',left:'30%',ease: Power2.easeOut,delay:2});
			TweenMax.to('.ani-plain-shadow',5, {width:'140px',left:'100%',ease: Power2.easeIn,delay:7.5,onComplete:_loopAni});
		}
	}
	if( _windowW <=1000){
		$(window).scroll(function(){
			var _scroll = $('body,html').scrollTop();
			//console.log(_scroll);
			if( _scroll >= 400){
				TweenMax.to('.img-1',1, {left:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 750){
				TweenMax.to('.img-4',1, {right:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 1120){
				TweenMax.to('.img-2',1, {left:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 1200){
				TweenMax.to('.img-3',1, {right:'0',ease: Power2.easeOut});
			}
		});
	}
	if( _windowW >=1000){
		$(window).scroll(function(){
			var _scroll = $('body,html').scrollTop();
			//console.log(_scroll);
			if( _scroll >= 600){
				TweenMax.to('.img-1',1, {left:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 1050){
				TweenMax.to('.img-4',1, {right:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 1600){
				TweenMax.to('.img-2',1, {left:'0',ease: Power2.easeOut});
			}
			if( _scroll >= 1800){
				TweenMax.to('.img-3',1, {right:'0',ease: Power2.easeOut});
			}
		});
	}
	
});